package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsMemberOauth;
import com.central.cms.service.CpCmsMemberOauthService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsMemberOauthController extends BaseController {

    @Autowired
    private CpCmsMemberOauthService cpcmsmemberoauthService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsMemberOauth cpcmsmemberoauth = cpcmsmemberoauthService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmemberoauth);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmemberoauthService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsMemberOauth cpcmsmemberoauth){
        cpcmsmemberoauthService.insertSelective(cpcmsmemberoauth);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsMemberOauth cpcmsmemberoauth){
        cpcmsmemberoauthService.updateSelectiveById(cpcmsmemberoauth);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsMemberOauth queryBean){
        PageInfo<CpCmsMemberOauth> page = cpcmsmemberoauthService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
