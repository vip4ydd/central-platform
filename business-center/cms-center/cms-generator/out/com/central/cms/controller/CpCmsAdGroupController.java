package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsAdGroup;
import com.central.cms.service.CpCmsAdGroupService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsAdGroupController extends BaseController {

    @Autowired
    private CpCmsAdGroupService cpcmsadgroupService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsAdGroup cpcmsadgroup = cpcmsadgroupService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsadgroup);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsadgroupService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsAdGroup cpcmsadgroup){
        cpcmsadgroupService.insertSelective(cpcmsadgroup);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsAdGroup cpcmsadgroup){
        cpcmsadgroupService.updateSelectiveById(cpcmsadgroup);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsAdGroup queryBean){
        PageInfo<CpCmsAdGroup> page = cpcmsadgroupService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
