package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsAdSilder;
import com.central.cms.service.CpCmsAdSilderService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsAdSilderController extends BaseController {

    @Autowired
    private CpCmsAdSilderService cpcmsadsilderService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsAdSilder cpcmsadsilder = cpcmsadsilderService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsadsilder);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsadsilderService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsAdSilder cpcmsadsilder){
        cpcmsadsilderService.insertSelective(cpcmsadsilder);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsAdSilder cpcmsadsilder){
        cpcmsadsilderService.updateSelectiveById(cpcmsadsilder);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsAdSilder queryBean){
        PageInfo<CpCmsAdSilder> page = cpcmsadsilderService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
