package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsSite;
import com.central.cms.service.CpCmsSiteService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsSiteController extends BaseController {

    @Autowired
    private CpCmsSiteService cpcmssiteService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsSite cpcmssite = cpcmssiteService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmssite);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmssiteService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsSite cpcmssite){
        cpcmssiteService.insertSelective(cpcmssite);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsSite cpcmssite){
        cpcmssiteService.updateSelectiveById(cpcmssite);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsSite queryBean){
        PageInfo<CpCmsSite> page = cpcmssiteService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
