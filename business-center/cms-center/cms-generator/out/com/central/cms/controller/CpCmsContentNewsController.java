package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsContentNews;
import com.central.cms.service.CpCmsContentNewsService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsContentNewsController extends BaseController {

    @Autowired
    private CpCmsContentNewsService cpcmscontentnewsService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsContentNews cpcmscontentnews = cpcmscontentnewsService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmscontentnews);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmscontentnewsService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsContentNews cpcmscontentnews){
        cpcmscontentnewsService.insertSelective(cpcmscontentnews);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsContentNews cpcmscontentnews){
        cpcmscontentnewsService.updateSelectiveById(cpcmscontentnews);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsContentNews queryBean){
        PageInfo<CpCmsContentNews> page = cpcmscontentnewsService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
