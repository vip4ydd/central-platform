package com.central.cms.service;

import com.central.cms.commons.base.service.BaseService;
import com.central.cms.mybatis.model.CpCmsAdmin;

public interface CpCmsAdminService extends BaseService<CpCmsAdmin>{

}
