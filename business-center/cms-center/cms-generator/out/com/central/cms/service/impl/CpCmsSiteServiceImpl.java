package com.central.cms.service.impl;

import com.central.cms.service.CpCmsSiteService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsSite;
import com.central.cms.mybatis.mapper.CpCmsSiteMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsSiteServiceImpl extends BaseServiceImpl<CpCmsSiteMapper, CpCmsSite> implements CpCmsSiteService {


}
