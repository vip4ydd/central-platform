package com.central.cms.service;

import com.central.cms.commons.base.service.BaseService;
import com.central.cms.mybatis.model.CpCmsModel;

public interface CpCmsModelService extends BaseService<CpCmsModel>{

}
