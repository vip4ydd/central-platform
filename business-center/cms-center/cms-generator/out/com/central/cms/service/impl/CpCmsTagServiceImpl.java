package com.central.cms.service.impl;

import com.central.cms.service.CpCmsTagService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsTag;
import com.central.cms.mybatis.mapper.CpCmsTagMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsTagServiceImpl extends BaseServiceImpl<CpCmsTagMapper, CpCmsTag> implements CpCmsTagService {


}
